Feature: General Manager views draft list
  As a General Manager
  So that I can research the players that I wish to draft
  I want to be able to view a list of all available players eligible to be drafted

  Scenario: General manager views all draftable players
    Given the following players exist:
      | Name              | Eligible? |
      | Joseph DelCioppio |  Yes      |
      | Steven Moretti    |  Yes      |
      | Michael Marchetti |  No       |
    When I visit the draft list
    Then I should see all players that can be drafted
