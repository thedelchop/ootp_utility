guard 'bundler' do
  watch('Gemfile')
end

# Start the spin server with RSpec and Cucumber support, and report time for each run
guard 'spin', cli: '--time -p', load_rspec: true, load_cucumber: true do
  # Spin itself
  watch('config/application.rb')
  watch('config/environment.rb')
  watch(%r{^config/environments/.*\.rb$})
  watch(%r{^config/initializers/.*\.rb$})
  watch(%r{^lib/(.+)\.rb$})
  watch('Gemfile')
  watch('Gemfile.lock')
end

guard 'livereload' do
  watch(%r{app/.+\.(erb|haml)})
  watch(%r{app/helpers/.+\.rb})
  watch(%r{(public/|app/assets).+\.(css|js|html)})
  watch(%r{(app/assets/.+\.css)\.s[ac]ss}) { |m| m[1] }
  watch(%r{(app/assets/.+\.js)\.coffee}) { |m| m[1] }
  watch(%r{config/locales/.+\.yml})
end

guard 'rails', port: 4000, server: 'thin', force_run: true, daemon: true do
  watch('Gemfile.lock')
  watch(%r{^(config|lib)/.*})
end

guard 'ctags-bundler' do
  watch(%r{^(app|lib|spec/support)/.*\.rb$})  { ["app", "lib", "spec/support"] }
  watch('Gemfile.lock')
end
