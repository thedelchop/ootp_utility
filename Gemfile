source 'https://rubygems.org'

gem 'rails', '3.2.8'

gem 'pg', '~> 0.14.1', group: :production

group :development do
  # Thin server and quite assets for much easier log readability
  gem 'thin',         '~> 1.5.0'
  gem 'quiet_assets', '~> 1.0.1'
  gem 'sqlite3'
end

# Gems used only for assets and not required
# in production environments by default.
group :assets do

  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'
  gem 'uglifier',     '>= 1.0.3'

  gem 'jquery-rails', '~> 2.1.3'
  gem 'ember-rails',  '~> 0.7.0'
end

group :development,:test do

  # Pry is a nice drop in for irb, which allows for debugging
  # of your code anywhere 'binding.pry' is included
  gem 'pry',        '~> 0.9.10'
  gem 'pry-remote', '~> 0.1.6'

  # Guard for file monitoring
  gem 'rb-fsevent',           '~> 0.9.1'
  gem 'rb-readline',          '~> 0.4.2'
  gem 'guard',                '~> 1.2.3'
  gem 'guard-bundler',        '~> 1.0.0'
  gem 'guard-livereload',     '~> 1.0.0'
  gem 'guard-ctags-bundler',  '~> 0.1.1'
  gem 'guard-rails',          '~> 0.1.0'

  gem "spin",                 github: "rickyrobinson/spin",               branch: "cucumber"
  gem "guard-spin",           github: "rickyrobinson/guard-spin",         branch: "cucumber"
end

group :test do
  #DatabaseCleaner keeps the database in a good state
  gem 'database_cleaner', '~> 0.7.2'

  #Factory girl for using factories instead of fixtures
  gem 'factory_girl',   '~> 4.1.0'

  #Cucumber for better acceptance testing
  gem 'cucumber-rails', '~> 1.3.0', require: false

  #Rspec for testing instead of test::unit
  gem "rspec-rails",    '~> 2.11.0'
end

# To use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# Use unicorn as the app server
# gem 'unicorn'

# Deploy with Capistrano
# gem 'capistrano'

# To use debugger
# gem 'debugger'
